$(document).ready(function(){
		

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var idPayment = null;
  
  var gObjectPayment = {
    checkNumber : "",
    paymentDate : "",
    ammount : "",
  };

  var STT = 0 ;
  const ARRAY_COLUM = ["id", "checkNumber", "paymentDate", "ammount", "action"];
  const ID_COL = 0;
  const CHECK_COL = 1;
  const DATE_COL = 2;
  const AMMOUNT_COL = 3;
  const ACTION_COL = 4;

  var table = $("#Payment-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[CHECK_COL]},
      {"data": ARRAY_COLUM[DATE_COL]},
      {"data": ARRAY_COLUM[AMMOUNT_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]}
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
  })
  var tableCustomer = $("#Customer-table-modal").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "lastName"},
      {"data": "firstName"},
      {"data": "phoneNumber"},
      {"data": "address"},
      {"data": "city"},
      {"data": "state"},
      {"data": "postalCode"},
      {"data": "country"},
      {"data": "salesRepEmployeeNumber"},
      {"data": "creditLimit"},
    ],
    "columnDefs": [
      {
              "targets": "id" , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    getPaymentAddToTable();
}


  // show modal Chi Tiết
  $("#btn-show-Customer").on("click", function(){
    $("#myModal-show-Customer").modal("show");
    showCustomerClick();
  })

  // sự kiện update click
  $("#save_data").on("click", function(){
    updatePaymentClick();
  });


  // sự kiện xoa click
  $("#Payment-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    addNewPaymentClick();
  });


  // Sự kiện click vào dòng trên bảng
  $("#Payment-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });

  // Sự kiện click vào dòng trên bảng customer
  $("#Customer-table-modal tbody").on("click","tr" , function(){
    onRowCustomerClick(this);
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function showCustomerClick(){

    inpModalCustomer();
  }

  // sự kiện thêm mới
  function addNewPaymentClick(){
    console.log("ok")
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectPayment);
    // B2 Validate
    var vCheck = validateDataForm(gObjectPayment);
    if(vCheck){
      $("#myModal-show-table-customer").modal("show");
      getAllCustomer();
    }
    
  }


  // Sự Kiện Xóa Payment
  function deleteClick(){
    "use strict"
    if(idPayment != null){

      
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/Payments/" + idPayment,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // updatePayment click
  function updatePaymentClick(){
    if(idPayment != null){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectPayment);
      // B2 Validate
      var vCheck = validateDataForm(gObjectPayment);
      if(vCheck){
        callApiUpdatePayment(gObjectPayment);
      }
    }else{
      alert("chưa chọn id muốn sửa");
    }
    
    
  }

  function onRowClick(e){
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#Payment-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow);
    idPayment = vDataRow.id;
    $("#input-Payment-checkNumber").val(vDataRow.checkNumber);
    $("#input-Payment-paymentDate").val(vDataRow.paymentDate);
    $("#input-ammount").val(vDataRow.ammount);
    $("#input-ID-code").val(vDataRow.id);
    $.ajax({
      url: "http://localhost:8080/customers/info/" + vDataRow.customerID,
      type: "GET",
      dataType: "json",
      success: function(res){
        inpModalCustomer(res);
        $("#input-Customer").val(res.firstName + " " + res.lastName);
      }
    });
    // console.log($(e.target).closest("tr").find("td:eq(0)").text())
  }
  function onRowCustomerClick(e){
    var vRow1Click = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable1 = $("#Customer-table-modal").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow1 = vTable1.row(vRow1Click).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow1);
    callApiAddNewPayment(vDataRow1.id);
    // console.log($(e.target).closest("tr").find("td:eq(0)").text())
  }
    
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update Payment
  function callApiUpdatePayment(data){
    "use strict";
    console.log(data);
    console.log(idPayment);
    $.ajax({
    url: "http://localhost:8080/Payments/" + idPayment,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập Payment thành công")
      console.log(res);
      getPaymentAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp Payment
  function readDataAddNewForm(data){
    "use strict"
    data.checkNumber = $("#input-Payment-checkNumber").val().trim();
    data.paymentDate = $("#input-Payment-paymentDate").val().trim();
    data.ammount = $("#input-ammount").val().trim();
  }
  // Validate du lieu
  function validateDataForm(data){
    "use strict"
    if(data.checkNumber == ""){
      alert("Chưa nhập checkNumber")
      return false;
    };
    if(data.paymentDate == ""){
      alert("Chưa nhập paymentDate")
      return false;
    }
    if(data.ammount == ""){
      alert("Chưa nhập ammount")
      return false;
    }
    
    return true ;

  }
    
  //call api add new Payment
  function callApiAddNewPayment(dataID){
    console.log(gObjectPayment);
    
    "use strict";
    $.ajax({
    url: "http://localhost:8080/Payments/" + dataID,
    type: "POST",
    data: JSON.stringify(gObjectPayment),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới Payment thành công" + res.id);
      console.log(res);
      getPaymentAddToTable();
      $("#myModal-show-table-customer").modal("hide");
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  
  // lấy dữ liệu và add payment vào bảng
  function getPaymentAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/Payments",
        type: "GET",
        dataType: "json",
        success: function(res){
          console.log(res);
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

   function clearInp(){
      "use strict"
      $("#input-ID-code").val("");
      $("#input-Payment-checkNumber").val("");
      $("#input-ammount").val("");
      $("#input-Payment-paymentDate").val("");
      $("#input-Customer").val("");
   }

   function inpModalCustomer(data){
      "use strict"
      $("#input-Customer-lastName").val(data.lastName);
      $("#input-Customer-firstName").val(data.firstName);
      $("#input-phone").val(data.phoneNumber);
      $("#input-address").val(data.address);
      $("#input-city").val(data.city);
      $("#input-state").val(data.state);
      $("#input-postalCode").val(data.postalCode);
      $("#input-country").val(data.country);
      $("#input-salesRepEmployeeNumber").val(data.salesRepEmployeeNumber);
      $("#input-creditLimit").val(data.creditLimit);
      $("#input-ID-code-modal").val(data.id);
      $.ajax({
        url: "http://localhost:8080/Orders/list/" + data.id,
        type: "GET",
        dataType: "json",
        success: function(res){
          $("#input-Orders").val(res.length);
        }
      });
      $.ajax({
        url: "http://localhost:8080/Payments/list/" + data.id,
        type: "GET",
        dataType: "json",
        success: function(res){
          $("#input-Payment").val(res.length);
        }
      });
   }

   // lấy danh sách customer
   function getAllCustomer(){
    "use strict"
    $.ajax({
      url: "http://localhost:8080/customers",
      type: "GET",
      dataType: "json",
      success: function(res){
        console.log(res);
        tableCustomer.clear();
        tableCustomer.rows.add(res);
        tableCustomer.draw();
      },
      error: function(err){
        console.log(err.response);
      }
    })

   }
})