$(document).ready(function(){
  $("#validate-form").validate({
    rules: {
      lastName: "required",
      firstName: "required",
      phone: {
        digits: true,
        minlength: 8,
        maxlength: 12,
        required: true
      }
    },
    messages: {
      firstName: "Vui lòng nhập họ",
      lastName: "Vui lòng nhập tên",
      phone: {
        digits: "Vui lòng nhập Số",
        minlength: "số Điện Thoại ngắn vậy, chém gió ah?",
        maxlength: "số Điện Thoại Dài vậy, chém gió ah?",
        required: "Vui lòng điền số điện thoại"
      }
    }
  });

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var idCustomer = null;
  var idEmployee = null;
  
  var gObjectCustomer = {
    lastName : "",
    firstName : "",
    phoneNumber : "",
    address: "",
    city: "",
    state: "",
    postalCode: "",
    country: "",
    creditLimit: "",
  };

  var STT = 0 ;
  var tableModalOrder = $("#Customer-table-modal").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "comments"},
      {"data": "orderDate"},
      {"data": "requiredDate"},
      {"data": "shippedDate"},
      {"data": "status"},
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })
  var tableModalPayment = $("#Customer-table-modal-payment").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "checkNumber"},
      {"data": "paymentDate"},
      {"data": "ammount"}
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  const ARRAY_COLUM = ["id", "lastName", "firstName", "phoneNumber", "address", "city", "state","postalCode","country","creditLimit", "action"];
  const ID_COL = 0;
  const LAST_COL = 1;
  const FIRST_COL = 2;
  const PHONE_COL = 3;
  const ADDRESS_COL = 4;
  const CITY_COL = 5;
  const STATE_COL = 6;
  const POSTALCODE_COL = 7;
  const COUNTRY_COL = 8;
  const CREDIT_COL = 9;
  const ACTION_COL = 10;

  var table = $("#Customer-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[LAST_COL]},
      {"data": ARRAY_COLUM[FIRST_COL]},
      {"data": ARRAY_COLUM[PHONE_COL]},
      {"data": ARRAY_COLUM[ADDRESS_COL]},
      {"data": ARRAY_COLUM[CITY_COL]},
      {"data": ARRAY_COLUM[STATE_COL]},
      {"data": ARRAY_COLUM[POSTALCODE_COL]},
      {"data": ARRAY_COLUM[COUNTRY_COL]},
      {"data": ARRAY_COLUM[CREDIT_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]}
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    $("#btn-show-Orders").hide();
    $("#btn-show-Payment").hide();
    getCustomerAddToTable();
}



  // show  Chi Tiết Hóa Đơn
  $("#btn-show-Payment").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show-payment").show();
    $("#section-show").hide();
  })
  // show  Chi Tiết Đơn Hàng
  $("#btn-show-Orders").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show-payment").hide();
    $("#section-show").show();
  })

  // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
  $(".btn-come-back").on("click", function(){
    $("#section-main").show();
    $("#section-show").hide();
    $("#section-show-payment").hide();
  });

  // sự kiện update click
  $("#save_data").on("click", function(){
    if( $("#validate-form").valid()){
      updateCustomerClick();
    }
  });


  // sự kiện xoa click
  $("#Customer-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    if( $("#validate-form").valid()){
      addNewCustomerClick();  
    }
  });
  // sự kiện hiện modal thêm mới click
  $("#btn-show-addNew").on("click", function(){
    clearInp();
    $(".Duong11").hide();
    $(".Duong22").show();
    $("#myModal-show-CRUD").modal("show");
  });


  // Sự kiện click vào dòng trên bảng
  $("#Customer-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // sự kiện thêm mới
  function addNewCustomerClick(){
    console.log("ok")
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectCustomer);
    callApiAddNewCustomer();
    
  }


  // Sự Kiện Xóa Customer
  function deleteClick(){
    "use strict"
    if(idCustomer != null){

      
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/customers/" + idCustomer,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // updateCustomer click
  function updateCustomerClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectCustomer);
      // Call Api Update
      callApiUpdateCustomer(gObjectCustomer);
  }

  function onRowClick(e){
    $(".Duong22").hide();
    $(".Duong11").show();
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#Customer-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow);
    idCustomer = vDataRow.id;
    $("#input-Customer-lastName").val(vDataRow.lastName);
    $("#input-Customer-firstName").val(vDataRow.firstName);
    $("#input-phone").val(vDataRow.phoneNumber);
    $("#input-address").val(vDataRow.address);
    $("#input-city").val(vDataRow.city);
    $("#input-state").val(vDataRow.state);
    $("#input-postalCode").val(vDataRow.postalCode);
    $("#input-country").val(vDataRow.country);
    $("#input-salesRepEmployeeNumber").val(vDataRow.employeeName);
    $("#input-creditLimit").val(vDataRow.creditLimit);
    $("#input-ID-code").val(vDataRow.id);
    $(".modal_h").text(vDataRow.lastName + " " + vDataRow.firstName);
    $.ajax({
      url: "http://localhost:8080/Orders/list/" + vDataRow.id,
      type: "GET",
      dataType: "json",
      success: function(res){
        $("#input-Orders").val(res.length);
        if(res.length > 0){
          $("#btn-show-Orders").show();
        }else{
          $("#btn-show-Orders").hide();
        }
        tableModalOrder.clear();
        tableModalOrder.rows.add(res);
        tableModalOrder.draw();
      }
    });
    $.ajax({
      url: "http://localhost:8080/Payments/list/" + vDataRow.id,
      type: "GET",
      dataType: "json",
      success: function(res){
        $("#input-Payment").val(res.length);
        if(res.length > 0){
          $("#btn-show-Payment").show();
        }else{
          $("#btn-show-Payment").hide();
        }
        tableModalPayment.clear();
        tableModalPayment.rows.add(res);
        tableModalPayment.draw();
      }
    });

    $("#myModal-show-CRUD").modal("show");
  }
    
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update Customer
  function callApiUpdateCustomer(data){
    "use strict";
    console.log(data);
    console.log(idCustomer);
    $.ajax({
    url: "http://localhost:8080/customers/" + idCustomer,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập Customer thành công")
      $("#myModal-show-CRUD").modal("hide");
      getCustomerAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp Customer
  function readDataAddNewForm(data){
    "use strict"
    data.lastName = $("#input-Customer-lastName").val().trim();
    data.firstName = $("#input-Customer-firstName").val().trim();
    data.phoneNumber = $("#input-phone").val().trim();
    data.address = $("#input-address").val().trim();
    data.city = $("#input-city").val().trim();
    data.state = $("#input-state").val().trim();
    data.postalCode = $("#input-postalCode").val().trim();
    data.country = $("#input-country").val().trim();
    data.creditLimit = $("#input-creditLimit").val().trim();
    if( $("#input-salesRepEmployeeNumber").val() == ""){
      idEmployee = 1 ;
    }else{
      idEmployee = $("#input-salesRepEmployeeNumber").val();
    }
  }
 

  //call api add new Customer
  function callApiAddNewCustomer(){
    console.log(gObjectCustomer);
    
    "use strict";
    $.ajax({
    url: "http://localhost:8080/customers/"+ idEmployee,
    type: "POST",
    data: JSON.stringify(gObjectCustomer),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới Customer thành công")
      getCustomerAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  

  function getCustomerAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/customers",
        type: "GET",
        dataType: "json",
        success: function(res){
          console.log(res);
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

   function clearInp(){
    "use strict"
    $("#input-Customer-lastName").val("");
    $("#input-Customer-firstName").val("");
    $("#input-phone").val("");
    $("#input-address").val("");
    $("#input-city").val("");
    $("#input-state").val("");
    $("#input-postalCode").val("");
    $("#input-country").val("");
    $("#input-salesRepEmployeeNumber").val("");
    $("#input-creditLimit").val("");
    $("#input-ID-code").val("");
    $("#input-Payment").val("");
    $("#input-Orders").val("");
   }
})