$(document).ready(function(){
  $("#validate-form").validate({
    rules: {
      comments: "required",
      customer: "required",
    },
    messages: {
      comments: "Hãy Để Lại Bình Luận !!!",
        customer: "Vui lòng Chọn Tên Khách Hàng !!!",
    }
  });

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var gOrderId = null;
  var gCustomerId = null;
  
  var gObjectOrder = {
      comments : "",
      orderDate : "",
      requiredDate : "",
      shippedDate: "",
      status: "",
  };

  var STT = 0 ;
  var tableCustomer = $("#Customer-table").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "lastName"},
      {"data": "firstName"},
      {"data": "phoneNumber"},
      {"data": "address"},
      {"data": "city"}
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  });

  var tableModalOrderDetails = $("#orderDtails-table-modal").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "quantityOrder"},
      {"data": "priceEach"},
      {"data": "orderID"},
      {"data": "product_name"}
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  const ARRAY_COLUM = ["id", "comments", "orderDate", "requiredDate", "shippedDate", "status","customerName","action"]; 
  const ID_COL = 0;
  const COMMENTS_COL = 1;
  const DATE_COL = 2;
  const REQUIREDDATE_COL = 3;
  const SHIPPER_COL = 4;
  const STATUS_COL = 5;
  const CUSTOMER_NAME_COL = 6;
  const ACTION_COL = 7;

  var table = $("#Order-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[COMMENTS_COL]},
      {"data": ARRAY_COLUM[DATE_COL]},
      {"data": ARRAY_COLUM[REQUIREDDATE_COL]},
      {"data": ARRAY_COLUM[SHIPPER_COL]},
      {"data": ARRAY_COLUM[STATUS_COL]},
      {"data": ARRAY_COLUM[CUSTOMER_NAME_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]},
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    $("#btn-show-orderDtails").hide();
    getOrderAddToTable();
    getListCustomerApi();
}
  // show modal Chi Tiết
  $("#btn-show-orderDtails").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show").show();
  })

  // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
  $("#btn-come-back").on("click", function(){
    $("#section-main").show();
    $("#section-show").hide();
  });

  // sự kiện update click
  $("#save_data").on("click", function(){
    if( $("#validate-form").valid()){
      updateOrderClick();
    }
  });


  // sự kiện xoa click
  $("#Order-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    if( $("#validate-form").valid()){
      addNewOrderClick();  
    }
  });
  // sự kiện hiện modal thêm mới click
  $("#btn-show-addNew").on("click", function(){
    clearInp();
    $(".Duong11").hide();
    $(".Duong22").show();
    $("#myModal-show-CRUD").modal("show");
  });


  // Sự kiện click vào dòng trên bảng
  $("#Order-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });

  // Sự kiện click vào dòng trên bảng
  $("#Customer-table tbody").on("click","tr" , function(){
    onRowClickCustomer(this);
  });
  // Sự kiện click vào dòng show danh sach customer
  $("#btn-show-customerList").on("click", function(){
    $("#myModal-show-Customer").modal("show");
  });



  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // sự kiện thêm mới
  function addNewOrderClick(){
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectOrder);
    console.log(gObjectOrder);
    // B2 Gọi Api
    callApiAddNewOrder();
    
  }

  // Sự Kiện Xóa Order
  function deleteClick(){
    "use strict"
    if(gOrderId != null){
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/Orders/" + gOrderId,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // update Order click
  function updateOrderClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectOrder);
      // Call Api Update
      callApiUpdateOrder(gObjectOrder);
  }

  // Sự Kiện Click Dòng bất kì trên bảng
  function onRowClick(e){
    $("#validate-form").validate().resetForm();
    $(".Duong22").hide();
    $(".Duong11").show();
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#Order-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow);
    gOrderId = vDataRow.id;
    $("#input-Order-orderDate").val(vDataRow.orderDate);
    $("#input-Order-comments").val(vDataRow.comments);
    $("#input-requiredDate").val(vDataRow.requiredDate);
    $("#input-shippedDate").val(vDataRow.shippedDate);
    $("#input-customer").val(vDataRow.customerName);
    $("#select-status").val(vDataRow.status);
    $("#input-orderDtails").val(vDataRow.orderDetails.length);
    if(vDataRow.orderDetails.length > 0){
      $("#btn-show-orderDtails").show();
    }else{$("#btn-show-orderDtails").hide()};
    $("#input-Order-id").val(vDataRow.id);
    $("#modal_h").text(vDataRow.orderDate);
    $("#myModal-show-CRUD").modal("show");
    tableModalOrderDetails.clear();
    tableModalOrderDetails.rows.add(vDataRow.orderDetails);
    tableModalOrderDetails.draw();
  }
  // Sự Kiện Click Dòng bất kì trên bảng customer
  function onRowClickCustomer(e){
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#Customer-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    $("#input-customer").val(vDataRow.firstName + " " + vDataRow.lastName);
    gCustomerId = vDataRow.id;
    $("#myModal-show-Customer").modal("hide");
  }



  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update Order
  function callApiUpdateOrder(data){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/Orders/" + gOrderId,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập Order thành công")
      $("#myModal-show-CRUD").modal("hide");
      getOrderAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp Order
  function readDataAddNewForm(data){
    "use strict"
    data.orderDate = $("#input-Order-orderDate").val().trim();
    data.comments = $("#input-Order-comments").val().trim();
    data.shippedDate = $("#input-shippedDate").val().trim();
    data.requiredDate = $("#input-requiredDate").val().trim();
    data.shippedDate = $("#input-shippedDate").val().trim();
    data.status = $("#select-status :selected").val();
  }
 

  //call api add new Order
  function callApiAddNewOrder(){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/Orders/"+ gCustomerId,
    type: "POST",
    data: JSON.stringify(gObjectOrder),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới Order thành công")
      $("#myModal-show-CRUD").modal("hide");
      getOrderAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  
  // lấy danh sách Nhân Viên Thêm vào bảng
  function getOrderAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/Orders",
        type: "GET",
        dataType: "json",
        success: function(res){
          STT = 0 ;
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

  // Gọi APi lấy danh sách Dòng Sản Phẩm
   function getListCustomerApi(){
      "use strict"
      $.ajax({
        url: "http://localhost:8080/customers",
        type: "GET",
        dataType: "json",
        success: function(res){
          loadCustomerToModalTable(res);
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }
   // xóa inp modal order
   function clearInp(){
    "use strict"
    $("#input-Order-orderDate").val("");
    $("#input-Order-comments").val("");
    $("#input-requiredDate").val("");
    $("#input-shippedDate").val("");
    $("#input-customer").val("");
    $("#input-Order-id").val("");
    $("#input-orderDetails").val("");
    $("#imgInp").val("");
   }


   // Thêm dữ liệu customer vào bảng
   function loadCustomerToModalTable(data){
      tableCustomer.clear();
      tableCustomer.rows.add(data);
      tableCustomer.draw();
   }

})