$(document).ready(function(){
    $("#validate-form").validate({
      rules: {
        lastName: "required",
        firstName: "required",
        phone: {
          digits: true,
          minlength: 8,
          maxlength: 12,
          required: true
        },
        email:{
          required: true,
          email: true
        },
        age:{
            required: true,
            min: 18,
            max: 55
        },
        jobtitle: "required",
        address: "required",
      },
      messages: {
        firstName: "Vui lòng nhập họ !!!",
        lastName: "Vui lòng nhập tên !!!",
        phone: {
          digits: "Vui lòng nhập Số !!!",
          minlength: "số Điện Thoại ngắn vậy, chém gió ah? !!!",
          maxlength: "số Điện Thoại Dài vậy, chém gió ah? !!!",
          required: "Vui lòng điền số điện thoại !!!"
        },
        email:{
            required: "Vui lòng Điền email !!!",
            email: "vui lòng nhập đúng email !!!"
          },
        age:{
            required: "Vui lòng nhập tuổi  !!!",
            min: "số tuổi quá nhỏ để làm việc !!!",
            max: "số tuổi quá lớn cần được nghỉ ngơi !!!"
        },
        jobtitle: "Vui Lòng điền chức vụ !!!",
        address: "Vui lòng nhập địa chỉ !!!",
      }
    });
  
    //Vùng 1: định nghĩa global variable (biến toàn cục)
    var idEmployee = null;
    var gOfficeId = null;
    
    var gObjectEmployee = {
        firstName : "",
        lastName : "",
        phoneNumber : "",
        email: "",
        age: "",
        address: "",
        jobTitle: ""
    };
  
    var STT = 0 ;
    var tableModalCustomer = $("#Customer-table-modal").DataTable({
      "columns": [
        {"data": "id"},
        {"data": "firstName"},
        {"data": "lastName"},
        {"data": "phoneNumber"},
        {"data": "address"},
        {"data": "city"},
        {"data": "state"},
        {"data": "postalCode"},
        {"data": "country"},
        {"data": "creditLimit"}
      ],
      dom: "Bfrtip",
      buttons: ["copy", "csv", "excel", "pdf", "print"],
    })

    const ARRAY_COLUM = ["id","firstName","lastName","phoneNumber", "email", "jobTitle", "age", "address","action"];
    const ID_COL = 0;
    const LAST_COL = 1;
    const FIRST_COL = 2;
    const PHONE_COL = 3;
    const email_COL = 4;
    const jobTitle_COL = 5;
    const age_COL = 6;
    const address_COL = 7;
    const ACTION_COL = 8;
  
    var table = $("#Employee-table").DataTable({
      "columns": [
        {"data": ARRAY_COLUM[ID_COL]},
        {"data": ARRAY_COLUM[LAST_COL]},
        {"data": ARRAY_COLUM[FIRST_COL]},
        {"data": ARRAY_COLUM[PHONE_COL]},
        {"data": ARRAY_COLUM[email_COL]},
        {"data": ARRAY_COLUM[jobTitle_COL]},
        {"data": ARRAY_COLUM[age_COL]},
        {"data": ARRAY_COLUM[address_COL]},
        {"data": ARRAY_COLUM[ACTION_COL]}
      ],
      "columnDefs": [
        { 
                "targets": ACTION_COL , 
                "className": 'text-center',
                "defaultContent":
                  `
                  <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                  <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                  `
                },
        {
                "targets": ID_COL , 
                "render": function () {
                  STT ++
                  return STT;
                  }
                }
        
      ],
      dom: "Bfrtip",
      buttons: ["copy", "csv", "excel", "pdf", "print"],
    })
  
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageloading();
    // sự kiện load trang
    function onPageloading(){
      $("#btn-show-customer").hide();
      getEmployeeAddToTable();
      getListOfficeApi();
  }
    // show modal Chi Tiết
    $("#btn-show-customer").on("click", function(){
      $("#myModal-show-CRUD").modal("hide");
      $("#section-main").hide();
      $("#section-show").show();
    })

    // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
    $("#btn-come-back").on("click", function(){
      $("#section-main").show();
      $("#section-show").hide();
    });

    // sự kiện update click
    $("#save_data").on("click", function(){
      if( $("#validate-form").valid()){
        updateEmployeeClick();
      }
    });
  
  
    // sự kiện xoa click
    $("#Employee-table").on("click", ".btn-delete" , function(){
      deleteClick();
    });
    $("#btn_Delete").on("click", deleteClick);
  
  
    // sự kiện thêm mới click
    $("#add-new").on("click", function(){
      if( $("#validate-form").valid()){
        addNewEmployeeClick();  
      }
    });
    // sự kiện hiện modal thêm mới click
    $("#btn-show-addNew").on("click", function(){
      clearInp();
      $(".Duong11").hide();
      $(".Duong22").show();
      $("#myModal-show-CRUD").modal("show");
    });
  
  
    // Sự kiện click vào dòng trên bảng
    $("#Employee-table tbody").on("click","tr" , function(){
      onRowClick(this);
    });
  
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // sự kiện thêm mới
    function addNewEmployeeClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectEmployee);
      // B2 Gọi Api
      callApiAddNewEmployee();
      
    }
  
    // Sự Kiện Xóa Employee
    function deleteClick(){
      "use strict"
      if(idEmployee != null){
        if (confirm("Bạn chắc chắn muốn xóa không") == true) {
          $.ajax({
          url: "http://localhost:8080/Employees/" + idEmployee,
          type: 'DELETE',
          success: function(result) {
            alert("Bạn Đã Xóa Thành Công")
              location.reload();
            }
          });
              } else {
              text = "You canceled!";
              }
        }	else{
          alert("Bạn chưa chọn ID");
        }			
    }
    
    // update Employee click
    function updateEmployeeClick(){
        // B1 Thu Thap Du Lieu
        readDataAddNewForm(gObjectEmployee);
        // Call Api Update
        callApiUpdateEmployee(gObjectEmployee);
    }
  
    // Sự Kiện Click Dòng bất kì trên bảng
    function onRowClick(e){
      $("#validate-form").validate().resetForm();
      $(".Duong22").hide();
      $(".Duong11").show();
      var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
      var vTable = $("#Employee-table").DataTable(); // tạo biến truy xuất đến DataTbale
      var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
      console.log(vDataRow);
      idEmployee = vDataRow.id;
      $("#input-Employee-lastName").val(vDataRow.lastName);
      $("#input-Employee-firstName").val(vDataRow.firstName);
      $("#input-phone").val(vDataRow.phoneNumber);
      $("#input-Email").val(vDataRow.email);
      $("#input-age").val(vDataRow.age);
      $("#input-address").val(vDataRow.address);
      $("#input-jobtitle").val(vDataRow.jobTitle);
      $("#select-office").val(vDataRow.officeId);
      $("#input-customer").val(vDataRow.customer.length);
      if(vDataRow.customer.length > 0){
        $("#btn-show-customer").show();
      }else{$("#btn-show-customer").hide()};
      $("#input-ID-code").val(vDataRow.id);
      $("#modal_h").text(vDataRow.firstName + " " + vDataRow.lastName);
      $("#myModal-show-CRUD").modal("show");
      tableModalCustomer.clear();
      tableModalCustomer.rows.add(vDataRow.customer);
      tableModalCustomer.draw();
    }

      // Load Danh sách office vào select
    function loadOfficeToSelect(dataOffice){
        console.log(dataOffice); 
        for (var i = 0; i < dataOffice.length; i++) {
          $("#select-office").append(
            $("<option>", {
              value: dataOffice[i].id,
              text: dataOffice[i].addressLine,
            }).attr("city" , dataOffice[i].city)
          );
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  
    //call api update Employee
    function callApiUpdateEmployee(data){
      "use strict";
      $.ajax({
      url: "http://localhost:8080/Employees/" + idEmployee,
      type: "PUT",
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: function(res){
        alert("Cập nhập Employee thành công")
        $("#myModal-show-CRUD").modal("hide");
        getEmployeeAddToTable();
        clearInp();
      },
      error: function(err){
        console.log(err.response);
        alert("that bai")
      }
      })
    }
  
    // thu thap du lieu inp Employee
    function readDataAddNewForm(data){
      "use strict"
      data.lastName = $("#input-Employee-lastName").val().trim();
      data.firstName = $("#input-Employee-firstName").val().trim();
      data.phoneNumber = $("#input-phone").val().trim();
      data.email = $("#input-Email").val().trim();
      data.age = $("#input-age").val().trim();
      data.address = $("#input-address").val().trim();
      data.jobTitle = $("#input-jobtitle").val().trim();
      gOfficeId = $("#select-office :selected").val();
    }
   
  
    //call api add new Employee
    function callApiAddNewEmployee(){
      "use strict";
      $.ajax({
      url: "http://localhost:8080/Employees/"+ gOfficeId,
      type: "POST",
      data: JSON.stringify(gObjectEmployee),
      contentType: 'application/json',
      success: function(res){
        alert("Thêm Mới Employee thành công")
        $("#myModal-show-CRUD").modal("hide");
        getEmployeeAddToTable();
        clearInp();
      },
      error: function(err){
        console.log(err.response);
        alert("that bai")
      }
      })
    }
    
    // lấy danh sách Nhân Viên Thêm vào bảng
    function getEmployeeAddToTable(){
      "use strict"
        $.ajax({
          url: "http://localhost:8080/Employees",
          type: "GET",
          dataType: "json",
          success: function(res){
            STT = 0 ;
            table.clear();
            table.rows.add(res);
            table.draw();
          },
          error: function(err){
            console.log(err.response);
          }
        })
     }

    // Gọi APi lấy danh sách office
     function getListOfficeApi(){
        "use strict"
        $.ajax({
          url: "http://localhost:8080/Offices",
          type: "GET",
          dataType: "json",
          success: function(res){
            loadOfficeToSelect(res);
          },
          error: function(err){
            console.log(err.response);
          }
        })
     }
  
     function clearInp(){
      "use strict"
      $("#input-Employee-lastName").val("");
      $("#input-Employee-firstName").val("");
      $("#input-phone").val("");
      $("#input-Email").val("");
      $("#input-age").val("");
      $("#input-address").val("");
      $("#input-jobtitle").val("");
      $("#input-ID-code").val("");
      $("#input-Customers").val("");
     }
  })