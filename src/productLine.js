$(document).ready(function(){
  $("#validate-form").validate({
    rules: {
      ProductLine: "required",
      description: "required",
    },
    messages: {
      ProductLine: "Vui lòng nhập Dòng Sản Phẩm !!!",
      description: "Vui Lòng Nhập Mô Tả !!!",
    }
  });

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var gProductLineId = null;
  
  var gObjectProductLine = {
      productLine: "",
      description: "",
  };

  var STT = 0 ;
  var tableModalProduct = $("#Product-table-modal").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "productCode"},
      {"data": "productName"},
      {"data": "productDescripttion"},
      {"data": "photo"},
      {"data": "productScale"},
      {"data": "productVendor"},
      {"data": "quantityInStock"},
      {"data": "buyPrice"},
    ],
    columnDefs: [{
      "targets": 4,
      "data": 'photo',
      "render": function (data, type, row, meta) {
          return '<img style="max-width: 200px;" src="' + data + '" alt="' + data + '"height="100%" width="100%"/>';
        }
    }],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  const ARRAY_COLUM = ["id","productLine",  "description", "action"];
  const ID_COL = 0;
  const productLine_COL = 1;
  const description_COL = 2;
  const ACTION_COL = 3;

  var table = $("#ProductLine-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[productLine_COL]},
      {"data": ARRAY_COLUM[description_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]}
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    $("#btn-show-ProductLine").hide();
    getProductLineAddToTable();
}
  // show Chi Tiết Sản Phẩm
  $("#btn-show-Product").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-ProductLine").hide();
    $("#section-Product").show();
  })
    // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
    $("#btn-come-back").on("click", function(){
      $("#section-ProductLine").show();
      $("#section-Product").hide();
    });

  // sự kiện update click
  $("#save_data").on("click", function(){
    if( $("#validate-form").valid()){
      updateProductLineClick();
    }
  });

  // sự kiện xoa click
  $("#ProductLine-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    if( $("#validate-form").valid()){
      addNewProductLineClick();  
    }
  });
  // sự kiện hiện modal thêm mới click
  $("#btn-show-addNew").on("click", function(){
    clearInp();
    $(".Duong11").hide();
    $(".Duong22").show();
    $("#myModal-show-CRUD").modal("show");
  });


  // Sự kiện click vào dòng trên bảng
  $("#ProductLine-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // sự kiện thêm mới
  function addNewProductLineClick(){
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectProductLine);
    // B2 Gọi Api
    callApiAddNewProductLine();
    
  }

  // Sự Kiện Xóa ProductLine
  function deleteClick(){
    "use strict"
    if(gProductLineId != null){
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/ProductLines/" + gProductLineId,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // update ProductLine click
  function updateProductLineClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectProductLine);
      // Call Api Update
      callApiUpdateProductLine(gObjectProductLine);
  }

  // Sự Kiện Click Dòng bất kì trên bảng
  function onRowClick(e){
    clearInp();
    $("#validate-form").validate().resetForm();
    $(".Duong22").hide();
    $(".Duong11").show();
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#ProductLine-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow);
    gProductLineId = vDataRow.id;
    $("#input-ProductLine").val(vDataRow.productLine);
    $("#input-description").val(vDataRow.description);
    $("#input-Product").val(vDataRow.products.length);
    if(vDataRow.products.length > 0){
      $("#btn-show-Product").show();
    }else{$("#btn-show-Product").hide()};
    $("#input-ID-code").val(vDataRow.id);
    $("#modal_h").text(vDataRow.productLine);
    $("#myModal-show-CRUD").modal("show");
    tableModalProduct.clear();
    tableModalProduct.rows.add(vDataRow.products);
    tableModalProduct.draw();
  }


  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update ProductLine
  function callApiUpdateProductLine(data){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/ProductLines/" + gProductLineId,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập ProductLine thành công")
      $("#myModal-show-CRUD").modal("hide");
      getProductLineAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp ProductLine
  function readDataAddNewForm(data){
    "use strict"
    data.productLine = $("#input-ProductLine").val().trim();
    data.description = $("#input-description").val().trim();
  }
 

  //call api add new ProductLine
  function callApiAddNewProductLine(){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/ProductLines",
    type: "POST",
    data: JSON.stringify(gObjectProductLine),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới ProductLine thành công")
      $("#myModal-show-CRUD").modal("hide");
      getProductLineAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  
  // lấy danh sách Dòng sản Thêm vào bảng
  function getProductLineAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/ProductLines",
        type: "GET",
        dataType: "json",
        success: function(res){
          STT = 0 ;
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }


   function clearInp(){
    "use strict"
    $("#input-Product").val("");
    $("#input-ProductLine").val("");
    $("#input-description").val("");
    $("#input-ID-code").val("");
   }
})