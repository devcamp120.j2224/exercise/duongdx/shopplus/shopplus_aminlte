$(document).ready(function(){
    $("#validate-form").validate({
      rules: {
        city: "required",
        phone: {
          digits: true,
          minlength: 8,
          maxlength: 12,
          required: true
        },
        addressLine:"required",
        state:"required",
        country: "required",
        territory: "required",
      },
      messages: {
        city: "Vui lòng nhập Thành phố !!!",
        phone: {
          digits: "Vui lòng nhập Số !!!",
          minlength: "số Điện Thoại ngắn vậy, chém gió ah? !!!",
          maxlength: "số Điện Thoại Dài vậy, chém gió ah? !!!",
          required: "Vui lòng điền số điện thoại !!!"
        },
        addressLine: "Vui lòng Điền Dòng Địa chỉ !!!",
        state:"Vui lòng nhập Trạng Thái  !!!",
        country: "Vui Lòng Nhập Quốc Gia !!!",
        territory: "Vui lòng nhập Lãnh Thổ !!!",
      }
    });
  
    //Vùng 1: định nghĩa global variable (biến toàn cục)
    var gOfficeId = null;
    
    var gObjectOffice = {
        city : "",
        phone : "",
        addressLine: "",
        state: "",
        territory: "",
        country: ""
    };
  
    var STT = 0 ;
    var tableModalEmployee = $("#Employee-table-modal").DataTable({
      "columns": [
        {"data": "id"},
        {"data": "firstName"},
        {"data": "lastName"},
        {"data": "phoneNumber"},
        {"data": "email"},
        {"data": "jobTitle"},
        {"data": "age"},
        {"data": "address"},
      ],
      dom: "Bfrtip",
      buttons: ["copy", "csv", "excel", "pdf", "print"],
    })

    const ARRAY_COLUM = ["id","city","phone", "addressLine",  "state", "country","territory","action"];
    const ID_COL = 0;
    const CITY_COL = 1;
    const PHONE_COL = 2;
    const addressLine_COL = 3;
    const state_COL = 4;
    const country_COL = 5;
    const territory_COL = 6;
    const ACTION_COL = 7;
  
    var table = $("#Office-table").DataTable({
      "columns": [
        {"data": ARRAY_COLUM[ID_COL]},
        {"data": ARRAY_COLUM[CITY_COL]},
        {"data": ARRAY_COLUM[PHONE_COL]},
        {"data": ARRAY_COLUM[addressLine_COL]},
        {"data": ARRAY_COLUM[state_COL]},
        {"data": ARRAY_COLUM[country_COL]},
        {"data": ARRAY_COLUM[territory_COL]},
        {"data": ARRAY_COLUM[ACTION_COL]}
      ],
      "columnDefs": [
        { 
                "targets": ACTION_COL , 
                "className": 'text-center',
                "defaultContent":
                  `
                  <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                  <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                  `
                },
        {
                "targets": ID_COL , 
                "render": function () {
                  STT ++
                  return STT;
                  }
                }
        
      ],
      dom: "Bfrtip",
      buttons: ["copy", "csv", "excel", "pdf", "print"],
    })
  
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageloading();
    // sự kiện load trang
    function onPageloading(){
      $("#btn-show-Office").hide();
      getOfficeAddToTable();
  }
  
      // show modal Chi Tiết
      $("#btn-show-Employee").on("click", function(){
        $("#myModal-show-CRUD").modal("hide");
        $("#section-main").hide();
        $("#section-show").show();
      })
  
      // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
      $("#btn-come-back").on("click", function(){
        $("#section-main").show();
        $("#section-show").hide();
      });

    // sự kiện update click
    $("#save_data").on("click", function(){
      if( $("#validate-form").valid()){
        updateOfficeClick();
      }
    });
  
    // sự kiện xoa click
    $("#Office-table").on("click", ".btn-delete" , function(){
      deleteClick();
    });
    $("#btn_Delete").on("click", deleteClick);
  
  
    // sự kiện thêm mới click
    $("#add-new").on("click", function(){
      if( $("#validate-form").valid()){
        addNewOfficeClick();  
      }
    });
    // sự kiện hiện modal thêm mới click
    $("#btn-show-addNew").on("click", function(){
      clearInp();
      $(".Duong11").hide();
      $(".Duong22").show();
      $("#myModal-show-CRUD").modal("show");
    });
  
  
    // Sự kiện click vào dòng trên bảng
    $("#Office-table tbody").on("click","tr" , function(){
      onRowClick(this);
    });
  
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // sự kiện thêm mới
    function addNewOfficeClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectOffice);
      // B2 Gọi Api
      callApiAddNewOffice();
      
    }
  
    // Sự Kiện Xóa Office
    function deleteClick(){
      "use strict"
      if(gOfficeId != null){
        if (confirm("Bạn chắc chắn muốn xóa không") == true) {
          $.ajax({
          url: "http://localhost:8080/Offices/" + gOfficeId,
          type: 'DELETE',
          success: function(result) {
            alert("Bạn Đã Xóa Thành Công")
              location.reload();
            }
          });
              } else {
              text = "You canceled!";
              }
        }	else{
          alert("Bạn chưa chọn ID");
        }			
    }
    
    // update Office click
    function updateOfficeClick(){
        // B1 Thu Thap Du Lieu
        readDataAddNewForm(gObjectOffice);
        // Call Api Update
        callApiUpdateOffice(gObjectOffice);
    }
  
    // Sự Kiện Click Dòng bất kì trên bảng
    function onRowClick(e){
      $("#validate-form").validate().resetForm();
      $(".Duong22").hide();
      $(".Duong11").show();
      var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
      var vTable = $("#Office-table").DataTable(); // tạo biến truy xuất đến DataTbale
      var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
      console.log(vDataRow);
      gOfficeId = vDataRow.id;
      $("#input-Office-city").val(vDataRow.city);
      $("#input-phone").val(vDataRow.phone);
      $("#input-addressLine").val(vDataRow.addressLine);
      $("#input-state").val(vDataRow.state);
      $("#input-territory").val(vDataRow.territory);
      $("#input-country").val(vDataRow.country);
      $("#input-Employee").val(vDataRow.employee.length);
      if(vDataRow.employee.length > 0){
        $("#btn-show-Employee").show();
      }else{$("#btn-show-Employee").hide()};
      $("#input-ID-code").val(vDataRow.id);
      $("#modal_h").text(vDataRow.addressLine);
      $("#myModal-show-CRUD").modal("show");
      tableModalEmployee.clear();
      tableModalEmployee.rows.add(vDataRow.employee);
      tableModalEmployee.draw();
    }


    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  
    //call api update Office
    function callApiUpdateOffice(data){
      "use strict";
      $.ajax({
      url: "http://localhost:8080/Offices/" + gOfficeId,
      type: "PUT",
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: function(res){
        alert("Cập nhập Office thành công")
        $("#myModal-show-CRUD").modal("hide");
        getOfficeAddToTable();
        clearInp();
      },
      error: function(err){
        console.log(err.response);
        alert("that bai")
      }
      })
    }
  
    // thu thap du lieu inp Office
    function readDataAddNewForm(data){
      "use strict"
      data.city = $("#input-Office-city").val().trim();
      data.phone = $("#input-phone").val().trim();
      data.addressLine = $("#input-addressLine").val().trim();
      data.state = $("#input-state").val().trim();
      data.territory = $("#input-territory").val().trim();
      data.country = $("#input-country").val().trim();
    }
   
  
    //call api add new Office
    function callApiAddNewOffice(){
      "use strict";
      $.ajax({
      url: "http://localhost:8080/Offices",
      type: "POST",
      data: JSON.stringify(gObjectOffice),
      contentType: 'application/json',
      success: function(res){
        alert("Thêm Mới Office thành công")
        $("#myModal-show-CRUD").modal("hide");
        getOfficeAddToTable();
        clearInp();
      },
      error: function(err){
        console.log(err.response);
        alert("that bai")
      }
      })
    }
    
    // lấy danh sách Nhân Viên Thêm vào bảng
    function getOfficeAddToTable(){
      "use strict"
        $.ajax({
          url: "http://localhost:8080/Offices",
          type: "GET",
          dataType: "json",
          success: function(res){
            STT = 0 ;
            table.clear();
            table.rows.add(res);
            table.draw();
          },
          error: function(err){
            console.log(err.response);
          }
        })
     }


     function clearInp(){
      "use strict"
      $("#input-Office-city").val("");
      $("#input-phone").val("");
      $("#input-addressLine").val("");
      $("#input-state").val("");
      $("#input-territory").val("");
      $("#input-country").val("");
      $("#input-ID-code").val("");
     }
  })