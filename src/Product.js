$(document).ready(function(){
  $("#validate-form").validate({
    rules: {
      productName: "required",
      productCode: "required",
      description: {
        required: true
      },
      Scale:{
        required: true,
      },
      Vendor:{
          required: true
      },
      buyPrice: "required",
      quantityInStock: "required",
      photo: "required",
    },
    messages: {
      productCode: "Vui lòng nhập Mã !!!",
      productName: "Vui lòng nhập tên !!!",
      description: {
        required: "Vui lòng điền Mô Tả Về Sản Phẩm !!!"
      },
      Scale:{
          required: "Vui lòng Điền Quy Mô Của Sản Phẩm !!!",
        },
      Vendor:{
          required: "Vui lòng nhập Nhà Cung Cấp !!!",
      },
      buyPrice: "Vui Lòng điền Giá !!!",
      quantityInStock: "Vui lòng nhập Số Lượng Có Trong Kho !!!",
      photo: "Vui lòng nhập Đường Dẫn Ảnh !!!",
    }
  });

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var gProductId = null;
  var gProductLineId = null;
  
  var gObjectProduct = {
      productCode : "",
      productName : "",
      productDescripttion : "",
      productScale: "",
      productVendor: "",
      quantityInStock: "",
      buyPrice: "",
      photo: ""
  };

  var STT = 0 ;
  var tableModalOrderDetails = $("#orderDtails-table-modal").DataTable({
    "columns": [
      {"data": "id"},
      {"data": "quantityOrder"},
      {"data": "priceEach"},
      {"data": "orderID"},
      {"data": "product_name"}
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  const ARRAY_COLUM = ["id", "productCode", "productName", "productDescripttion", "productLineName", "productScale", "productVendor","quantityInStock","buyPrice","photo","action"];
  const ID_COL = 0;
  const LAST_COL = 1;
  const FIRST_COL = 2;
  const PHONE_COL = 3;
  const idProductLine_COL = 4;
  const productScale_COL = 5;
  const productVendor_COL = 6;
  const quantityInStock_COL = 7;
  const buyPrice_COL = 8;
  const PHOTO_COL = 9;
  const ACTION_COL = 10;

  var table = $("#Product-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[LAST_COL]},
      {"data": ARRAY_COLUM[FIRST_COL]},
      {"data": ARRAY_COLUM[PHONE_COL]},
      {"data": ARRAY_COLUM[idProductLine_COL]},
      {"data": ARRAY_COLUM[productScale_COL]},
      {"data": ARRAY_COLUM[productVendor_COL]},
      {"data": ARRAY_COLUM[quantityInStock_COL]},
      {"data": ARRAY_COLUM[buyPrice_COL]},
      {"data": ARRAY_COLUM[PHOTO_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]}
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              },
      {
              "targets": PHOTO_COL,
              "data": 'photo',
              "render": function (data, type, row, meta) {
                  return '<img style="max-width: 200px;" src="' + data + '" alt="' + data + '"height="100%" width="100%"/>';
                }
              }
      
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    $("#btn-show-orderDtails").hide();
    getProductAddToTable();
    getListProductLineApi();
}
  // show modal Chi Tiết
  $("#btn-show-orderDtails").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show").show();
  })

  // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
  $("#btn-come-back").on("click", function(){
    $("#section-main").show();
    $("#section-show").hide();
  });

  // sự kiện update click
  $("#save_data").on("click", function(){
    if( $("#validate-form").valid()){
      updateProductClick();
    }
  });


  // sự kiện xoa click
  $("#Product-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    if( $("#validate-form").valid()){
      addNewProductClick();  
    }
  });
  // sự kiện hiện modal thêm mới click
  $("#btn-show-addNew").on("click", function(){
    clearInp();
    $(".Duong11").hide();
    $(".Duong22").show();
    $("#myModal-show-CRUD").modal("show");
  });


  // Sự kiện click vào dòng trên bảng
  $("#Product-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });



  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // sự kiện thêm mới
  function addNewProductClick(){
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectProduct);
    // B2 Gọi Api
    callApiAddNewProduct();
    
  }

  // Sự Kiện Xóa Product
  function deleteClick(){
    "use strict"
    if(gProductId != null){
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/Products/" + gProductId,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // update Product click
  function updateProductClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectProduct);
      // Call Api Update
      callApiUpdateProduct(gObjectProduct);
  }

  // Sự Kiện Click Dòng bất kì trên bảng
  function onRowClick(e){
    $("#validate-form").validate().resetForm();
    $(".Duong22").hide();
    $(".Duong11").show();
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#Product-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow);
    gProductId = vDataRow.id;
    $("#input-Product-productName").val(vDataRow.productName);
    $("#input-Product-productCode").val(vDataRow.productCode);
    $("#input-description").val(vDataRow.productDescripttion);
    $("#input-Scale").val(vDataRow.productScale);
    $("#input-Vendor").val(vDataRow.productVendor);
    $("#input-quantityInStock").val(vDataRow.quantityInStock);
    $("#input-buyPrice").val(vDataRow.buyPrice);
    $("#select-productLine").val(vDataRow.idProductLine);
    $("#imgInp").val(vDataRow.photo);
    $("#input-orderDtails").val(vDataRow.orderDetails.length);
    if(vDataRow.orderDetails.length > 0){
      $("#btn-show-orderDtails").show();
    }else{$("#btn-show-orderDtails").hide()};
    $("#input-Product-id").val(vDataRow.id);
    $("#modal_h").text(vDataRow.productName);
    $("#myModal-show-CRUD").modal("show");
    tableModalOrderDetails.clear();
    tableModalOrderDetails.rows.add(vDataRow.orderDetails);
    tableModalOrderDetails.draw();
  }

    // Load Danh sách ProductLine vào select
  function loadProductLineToSelect(Data){
      console.log(Data); 
      for (var i = 0; i < Data.length; i++) {
        $("#select-productLine").append(
          $("<option>", {
            value: Data[i].id,
            text: Data[i].productLine,
          })
        );
      }
  }

  // imgInp.onchange = evt => {
  //   const [file] = imgInp.files
  //   if (file) {
  //     blah.src = URL.createObjectURL(file)
  //   }
  // }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update Product
  function callApiUpdateProduct(data){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/Products/" + gProductId,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập Product thành công")
      $("#myModal-show-CRUD").modal("hide");
      getProductAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp Product
  function readDataAddNewForm(data){
    "use strict"
    data.productName = $("#input-Product-productName").val().trim();
    data.productCode = $("#input-Product-productCode").val().trim();
    data.productDescripttion = $("#input-description").val().trim();
    data.productScale = $("#input-Scale").val().trim();
    data.photo = $("#imgInp").val().trim();
    data.productVendor = $("#input-Vendor").val().trim();
    data.quantityInStock = $("#input-quantityInStock").val().trim();
    data.buyPrice = $("#input-buyPrice").val().trim();
    gProductLineId = $("#select-productLine :selected").val();
  }
 

  //call api add new Product
  function callApiAddNewProduct(){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/Products/"+ gProductLineId,
    type: "POST",
    data: JSON.stringify(gObjectProduct),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới Product thành công")
      $("#myModal-show-CRUD").modal("hide");
      getProductAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  
  // lấy danh sách Nhân Viên Thêm vào bảng
  function getProductAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/Products",
        type: "GET",
        dataType: "json",
        success: function(res){
          STT = 0 ;
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

  // Gọi APi lấy danh sách Dòng Sản Phẩm
   function getListProductLineApi(){
      "use strict"
      $.ajax({
        url: "http://localhost:8080/ProductLines",
        type: "GET",
        dataType: "json",
        success: function(res){
          loadProductLineToSelect(res);
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

   function clearInp(){
    "use strict"
    $("#input-Product-productName").val("");
    $("#input-Product-productCode").val("");
    $("#input-description").val("");
    $("#input-Scale").val("");
    $("#input-Vendor").val("");
    $("#input-quantityInStock").val("");
    $("#input-buyPrice").val("");
    $("#input-Product-id").val("");
    $("#input-orderDetails").val("");
    $("#imgInp").val("");
   }
})