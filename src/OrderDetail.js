$(document).ready(function(){
  $("#validate-form").validate({
    rules: {
      quantityOrder: {
        required: true,
        digits: true,
      },
      priceEach: {
        required: true,
        digits: true,
      },
      orderID: {
        required: true,
        digits: true,
      }
    },
    messages: {
      quantityOrder: {
        required: "Không Được Để Trống !",
        digits: "Phải là số dương !",
      },
      priceEach: {
        required: "Không Được Để Trống !",
        digits: "Phải là số dương !",
      },
      orderID: {
        required: "Không Được Để Trống !",
        digits: "Phải là số dương !",
      }
    }
  });

  //Vùng 1: định nghĩa global variable (biến toàn cục)
  var gOrderId = null;
  var gCustomerId = null;
  
  var gObjectOrder = {
      comments : "",
      quantityOrder : "",
      priceEach : "",
      orderID: "",
      status: "",
  };

  var STT = 0 ;

  const ARRAY_COLUM = ["id", "product_name", "quantityOrder", "priceEach", "orderID", "action"]; 
  const ID_COL = 0;
  const COMMENTS_COL = 1;
  const DATE_COL = 2;
  const priceEach_COL = 3;
  const SHIPPER_COL = 4;
  const ACTION_COL = 5;

  var table = $("#orderdtail-table").DataTable({
    "columns": [
      {"data": ARRAY_COLUM[ID_COL]},
      {"data": ARRAY_COLUM[COMMENTS_COL]},
      {"data": ARRAY_COLUM[DATE_COL]},
      {"data": ARRAY_COLUM[priceEach_COL]},
      {"data": ARRAY_COLUM[SHIPPER_COL]},
      {"data": ARRAY_COLUM[ACTION_COL]},
    ],
    "columnDefs": [
      { 
              "targets": ACTION_COL , 
              "className": 'text-center',
              "defaultContent":
                `
                <button type="button" class="btn btn-link btn-update" id="btn-update"><i class="fas fa-edit"></i></button>
                <button type="button" class="btn btn-link btn-delete" id="btn-delete"><i class="fas fa-trash-alt"></i></button>
                `
              },
      {
              "targets": ID_COL , 
              "render": function () {
                STT ++
                return STT;
                }
              }
      
    ],
    dom: "Bfrtip",
    buttons: ["copy", "csv", "excel", "pdf", "print"],
  })

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageloading();
  // sự kiện load trang
  function onPageloading(){
    // $("#btn-show-order").hide();
    getOrderAddToTable();
    getProductAddToSelect();
}
  // show modal Chi Tiết
  $("#btn-show-order").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show-product").hide();
    $("#section-show").show();
    GetOrderInfoById();
  })
  // show modal Chi Tiết
  $("#btn-show-product").on("click", function(){
    $("#myModal-show-CRUD").modal("hide");
    $("#section-main").hide();
    $("#section-show").hide();
    $("#section-show-product").show();
    GetProductInfoById();
  })

  // sự kiện Trỏ Lại Trang Dòng Sản Phẩm
  $(".btn-come-back").on("click", function(){
    $("#section-main").show();
    $("#section-show").hide();
    $("#section-show-product").hide();
  });

  // sự kiện update click
  $("#save_data").on("click", function(){
    if( $("#validate-form").valid()){
      updateOrderClick();
    }
  });


  // sự kiện xoa click
  $("#orderdtail-table").on("click", ".btn-delete" , function(){
    deleteClick();
  });
  $("#btn_Delete").on("click", deleteClick);


  // sự kiện thêm mới click
  $("#add-new").on("click", function(){
    if( $("#validate-form").valid()){
      addNewOrderClick();  
    }
  });
  // sự kiện hiện modal thêm mới click
  $("#btn-show-addNew").on("click", function(){
    clearInp();
    $(".Duong11").hide();
    $(".Duong22").show();
    $("#myModal-show-CRUD").modal("show");
  });


  // Sự kiện click vào dòng trên bảng
  $("#orderdtail-table tbody").on("click","tr" , function(){
    onRowClick(this);
  });




  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // sự kiện thêm mới
  function addNewOrderClick(){
    // B1 Thu Thap Du Lieu
    readDataAddNewForm(gObjectOrder);
    console.log(gObjectOrder);
    // B2 Gọi Api
    callApiAddNewOrder();
    
  }

  // Sự Kiện Xóa Order
  function deleteClick(){
    "use strict"
    if(gOrderId != null){
      if (confirm("Bạn chắc chắn muốn xóa không") == true) {
        $.ajax({
        url: "http://localhost:8080/orderdetails/" + gOrderId,
        type: 'DELETE',
        success: function(result) {
          alert("Bạn Đã Xóa Thành Công")
            location.reload();
          }
        });
            } else {
            text = "You canceled!";
            }
      }	else{
        alert("Bạn chưa chọn ID");
      }			
  }
  
  // update Order click
  function updateOrderClick(){
      // B1 Thu Thap Du Lieu
      readDataAddNewForm(gObjectOrder);
      // Call Api Update
      callApiUpdateOrder(gObjectOrder);
  }

  // Sự Kiện Click Dòng bất kì trên bảng
  function onRowClick(e){
    $("#validate-form").validate().resetForm();
    $(".Duong22").hide();
    $(".Duong11").show();
    var vRowClick = $(e).closest("tr"); // xác định tr chứa nút bấm được click
    var vTable = $("#orderdtail-table").DataTable(); // tạo biến truy xuất đến DataTbale
    var vDataRow = vTable.row(vRowClick).data(); // lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    gOrderId = vDataRow.id;
    gProductId = vDataRow.productID;
    $("#input-Order-quantityOrder").val(vDataRow.quantityOrder);
    $("#input-Order-priceEach").val(vDataRow.priceEach);
    $("#input-order-ID-modal").val(vDataRow.orderID);
    $("#select-product").val(vDataRow.productID);
    $("#input-orderdetails-id").val(vDataRow.id);
    $("#myModal-show-CRUD").modal("show");
  }


  // Load Danh sách ProductLine vào select
  function loadProductToSelect(Data){
    console.log(Data); 
    for (var i = 0; i < Data.length; i++) {
      $("#select-product").append(
        $("<option>", {
          value: Data[i].id,
          text: Data[i].productName,
        })
      );
    }
}

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  //call api update Order
  function callApiUpdateOrder(data){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/orderdetails/" + gOrderId,
    type: "PUT",
    data: JSON.stringify(data),
    contentType: 'application/json',
    success: function(res){
      alert("Cập nhập Order thành công")
      $("#myModal-show-CRUD").modal("hide");
      getOrderAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }

  // thu thap du lieu inp Order
  function readDataAddNewForm(data){
    "use strict"
    data.quantityOrder = $("#input-Order-quantityOrder").val().trim();
    data.priceEach = $("#input-Order-priceEach").val().trim();

  }
 

  //call api add new Order
  function callApiAddNewOrder(){
    "use strict";
    $.ajax({
    url: "http://localhost:8080/orderdetails/"+ $("#input-order-ID-modal").val() + "/" +  $("#select-product :selected").val(),
    type: "POST",
    data: JSON.stringify(gObjectOrder),
    contentType: 'application/json',
    success: function(res){
      alert("Thêm Mới Order thành công")
      $("#myModal-show-CRUD").modal("hide");
      getOrderAddToTable();
      clearInp();
    },
    error: function(err){
      console.log(err.response);
      alert("that bai")
    }
    })
  }
  
  // lấy danh sách Đơn Chi Tiết Thêm vào bảng
  function getOrderAddToTable(){
    "use strict"
      $.ajax({
        url: "http://localhost:8080/orderdetails",
        type: "GET",
        dataType: "json",
        success: function(res){
          console.log(res);
          STT = 0 ;
          table.clear();
          table.rows.add(res);
          table.draw();
        },
        error: function(err){
          console.log(err.response);
        }
      })
   }

   // xóa inp modal order
   function clearInp(){
    "use strict"
    $("#input-Order-quantityOrder").val("");
    $("#input-Order-priceEach").val("");
    $("#input-product_name").val("");
    $("#input-Order-id").val("");
    $("#input-order-ID-modal").val("");
   }

   // lấy thông tin order bằng id
   function GetOrderInfoById(){
    "use strict"
    $.ajax({
      url: "http://localhost:8080/Orders/info/" + $("#input-order-ID-modal").val(),
      type: "GET",
      dataType: "json",
      success: function(res){
        loadOrderToForm(res);
      },
      error: function(err){
        console.log(err.response);
      }
    })
   }
   // load dữ liệu vào bảng order
   function loadOrderToForm(data){
    "use strict"
    $("#input-Order-id").val(data.id);
    $("#input-Order-comments").val(data.comments);
    $("#input-Order-orderDate").val(data.orderDate);
    $("#input-requiredDate").val(data.requiredDate);
    $("#input-shippedDate").val(data.shippedDate);
    $("#input-customer").val(data.customerName);
    $("#input-orderDtails").val(data.orderDetails.length);
    $("#inp-status-order").val(data.status);
   }
   
   // lấy thông tin Product bằng id
   function GetProductInfoById(){
    "use strict"
    $.ajax({
      url: "http://localhost:8080/Products/info/" + gProductId,
      type: "GET",
      dataType: "json",
      success: function(res){
        loadProductToForm(res);
      },
      error: function(err){
        console.log(err.response);
      }
    })
   }
   // load dữ liệu vào bảng Product
   function loadProductToForm(data){
    "use strict"
    $("#input-Product-id").val(data.id);
    $("#input-productCode").val(data.productCode);
    $("#input-productName").val(data.productName);
    $("#input-productDescripttion").val(data.productDescripttion);
    $("#img-photo").attr("src", data.photo);
    $("#input-productScale").val(data.productScale);
    $("#inp-orderDetails-Product").val(data.orderDetails.length);
    $("#input-productVendor").val(data.productVendor);
    $("#input-quantityInStock").val(data.quantityInStock);
    $("#inp-buyPrice-Product").val(data.buyPrice);
    $("#inp-productLineName-Product").val(data.productLineName);
   }
   
   // Lấy danh sách Product Thêm vào select
   function getProductAddToSelect(){
    "use strict"
    $.ajax({
      url: "http://localhost:8080/Products",
      type: "GET",
      dataType: "json",
      success: function(res){
        loadProductToSelect(res);
      },
      error: function(err){
        console.log(err.response);
      }
    })
   }
})